package decaf;
import org.antlr.symtab.FunctionSymbol;
import org.antlr.symtab.GlobalScope;
import org.antlr.symtab.LocalScope;
import org.antlr.symtab.Scope;
import org.antlr.symtab.VariableSymbol;
import org.antlr.symtab.Symbol;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import java.util.ArrayList;
import java.io.*;


/**
 * This class defines basic symbols and scopes for Decaf language
 */
public class DecafSymbolsAndScopes extends DecafParserBaseListener {
    ParseTreeProperty<Scope> scopes = new ParseTreeProperty<Scope>();
    GlobalScope globals;
    Scope currentScope;
    // define symbols in this scope
    //int cont = 0;
    /*ArrayList<String> nmReservado = new ArrayList();
    ArrayList<String> metodosDisponiveis = new ArrayList();
    ArrayList<String> variaveisGlobais = new ArrayList();
    ArrayList<String> variaveisDecl = new ArrayList();
    */
    //ArrayList<String> parametro = new ArrayList();

    @Override
    public void enterProgram(DecafParser.ProgramContext ctx) {
        globals = new GlobalScope(null);
        //System.out.println("--------enter program------");
        //System.out.println(globals);
        pushScope(globals);
        //System.out.println(currentScope.getName());
        //System.out.println("--------enter program------");
    }

    @Override
    public void exitProgram(DecafParser.ProgramContext ctx) {
        if (!verificarVar("main"))
            System.out.println("Não existir o metodo main");
        popScope();
       // if(!metodosDisponiveis.contains("main"))
        //System.out.println(currentScope.getName());
       //     System.out.println("Não existir metodo main");
    }

    @Override
    public void enterMethod_decl(DecafParser.Method_declContext ctx) {
        //System.out.println("------method-decl------");
        //System.out.println("1");
        String name = ctx.ID().getText();
        //nmReservado.add(name);
        //metodosDisponiveis.add(name);
        //System.out.println("2");
        //System.out.println(name);
        int typeTokenType = ctx.type().start.getType();
        //System.out.println("3");
        //System.out.println(typeTokenType);
        DecafSymbol.Type type = this.getType(typeTokenType);
        //System.out.println("4");
        //System.out.println(type);
        // push new scope by making new one that points to enclosing scope
        FunctionSymbol function = new FunctionSymbol(name);
        //System.out.println("5");
        //System.out.println(function);
        //System.out.println(ctx);
        // function.setType(type); // Set symbol type

        currentScope.define(function); // Define function in current scope
        //System.out.println("6");
        saveScope(ctx, function);
        //System.out.println("7");
        pushScope(function);
        //System.out.println("8");
        //System.out.println("------method-decl------");
    }

    @Override
    public void exitMethod_decl(DecafParser.Method_declContext ctx) {
        //System.out.println("--------method-decl--------*");
        //System.out.println(ctx);
        popScope();
        //variaveisDecl.add("-");
        //variaveisLocal.add("-");
        //System.out.println("---------method-decl---------*");
    }

    @Override
    public void enterBlock(DecafParser.BlockContext ctx) {
        LocalScope l = new LocalScope(currentScope);
        //System.out.println("----------block---------");
        //System.out.println(l);
        //System.out.println(currentScope);
        saveScope(ctx, currentScope);
        //System.out.println("----------block---------");
        // pushScope(l);
    }

    @Override
    public void enterLocation(DecafParser.LocationContext ctx)  {
        //System.out.println("------------location----------");
        //if (!verificarVar(ctx.ID().getText())) {
            //System.out.println("Variavel inexsistente no escopo atual");
          //  System.out.println(ctx.ID().getText());
       // }
        verificarVarGlobal(ctx.ID().getText());
    }

    @Override
    public void enterField_suport(DecafParser.Field_suportContext ctx) {
        //System.out.println("----------filedsuport------------");
        if (ctx.NUMBER() != null) {
            if (Integer.parseInt(ctx.NUMBER().getText()) <= 0)
                error(ctx.NUMBER().getSymbol(), "Vetor não pode possuir o valor 0");
            //System.out.println("aaa");
        }
    }



    boolean verificarVar(String nomeID){
        boolean valor = false;
        String [] a, b;
        if (!currentScope.getName().equals("global")) {
            a = String.valueOf(currentScope).split(currentScope.getName());
            b = a[1].split("\\[");
        }else {
            b = String.valueOf(currentScope).split("\\[");

        }

        //System.out.println(b[1]);
        if (!b[1].equals("]")) {
            String[] c = b[1].split("\\]");
            //System.out.println(c[0]);
            String[] e = c[0].split("\\,");
            //System.out.println(e.length);
            for (int i = 0; i < e.length; i++) {
                //System.out.println(e[i]);
                if (e[i].trim().equals(nomeID)) {
                    //System.out.println("existe");
                    valor = true;
                    break;
                }
            }

        }
        //System.out.println(valor);
        return valor;
        /*System.out.println(e.length);
        System.out.println(e[0]);
        System.out.println(e[1]);*/
    }

    boolean verificarVarGlobal(String nomeID){
        boolean valor = false;
        String [] a, b;
        //System.out.println(String.valueOf(globals));
        b = String.valueOf(globals).split("\\[");
        //System.out.println(b);
        //System.out.println(b[1]);

        /*if (!b[1].equals("]")) {
            String[] c = b[1].split("\\]");
            //System.out.println(c[0]);
            String[] e = c[0].split("\\,");
            //System.out.println(e.length);
            for (int i = 0; i < e.length; i++) {
                //System.out.println(e[i]);
                if (e[i].trim().equals(nomeID)) {
                    //System.out.println("existe");
                    valor = true;
                    break;
                }
            }

        }
        System.out.println(valor);
        */
        return valor;
        /*System.out.println(e.length);
        System.out.println(e[0]);
        System.out.println(e[1]);*/

    }

    boolean verificarMethod(String nomeID){
        boolean valor = false;
        String [] a, b;
        //System.out.println("----------verificar metodo----------");
        //System.out.println(currentScope);
        b = String.valueOf(currentScope).split("\\[");
        //System.out.println(b[1]);
        if (!b[1].equals("]")) {
            String[] c = b[1].split("\\]");
            //System.out.println(c[0]);
            String[] e = c[0].split("\\,");
            //System.out.println(e.length);
            for (int i = 0; i < e.length; i++) {
                //System.out.println(e[i]);
                if (e[i].trim().equals(nomeID)) {
                    //System.out.println("existe");
                    valor = true;
                    break;
                }
            }

        }
        //System.out.println("--------sair do metodo--------");
        return valor;

    }
/*
    public void enterMethod_call(DecafParser.Method_callContext ctx){
        //System.out.println(globals.getName());
    }

    @Override
    public void enterMethod_decl_var(DecafParser.Method_decl_varContext ctx) {
        //currentScope.define(n"-");

    }

    int typeTokenType = ctx.type().start.getType();
        DecafSymbol.Type type = this.getType(typeTokenType);

        // push new scope by making new one that points to enclosing scope
        FunctionSymbol function = new FunctionSymbol(name);
        // function.setType(type); // Set symbol type

        void defineVar(DecafParser.TypeContext typeCtx, Token nameToken) {
        int typeTokenType = typeCtx.start.getType();
        VariableSymbol var = new VariableSymbol(nameToken.getText());

        // DecafSymbol.Type type = this.getType(typeTokenType);
        // var.setType(type);

        currentScope.define(var); // Define symbol in current scope
    }

    @Override
    public void exitMethod_decl_var(DecafParser.Method_decl_varContext ctx) {
        //currentScope.define(n"-");

    }
*/
    public void enterDecl_var(DecafParser.Decl_varContext ctx) {
        String tipo ="";
        //System.out.println("----------declvar---------");
        //System.out.println("declvar");
        //System.out.println(currentScope);
        try{
            if (ctx.INT() != null){

                tipo = ctx.INT().getSymbol().getText();
            }
            if (ctx.BOOLEAN() != null){
                tipo = ctx.BOOLEAN().getSymbol().getText();}
        } catch(Exception e){
            tipo = "";
        }
        defineVar(tipo, ctx.ID().getSymbol());

    }

    @Override
    public void exitDecl_var(DecafParser.Decl_varContext ctx) {
        String name = ctx.ID().getSymbol().getText();
        Symbol var = currentScope.resolve(name);
        //System.out.println("----------decl-var--------*");
        //System.out.println(name);
        //System.out.println(var);
        if ( var==null ) {
            this.error(ctx.ID().getSymbol(), "no such variable: "+name);
        }
        if ( var instanceof FunctionSymbol ) {
            this.error(ctx.ID().getSymbol(), name+" is not a variable");
        }
        //System.out.println("----------decl-var---------*");

    }

    void defineVar( String nameTipo, Token nameID) {
        //int typeTokenType = typeCtx.start.getType();
        VariableSymbol var = new VariableSymbol(nameID.getText());
        //System.out.println("---------define var--------");
        //System.out.println(typeTokenType);
        //System.out.println(var);
        // DecafSymbol.Type type = this.getType(typeTokenType);
        // var.setType(type);
        currentScope.define(var); // Define symbol in current scope
        //System.out.println("----------define var-------------");
    }

    /**
     * Método que atuliza o escopo para o atual e imprime o valor
     *
     * @param s
     */
    private void pushScope(Scope s) {
        currentScope = s;
        System.out.println("entering: "+currentScope.getName()+":"+s);
    }

    /**
     * Método que cria um novo escopo no contexto fornecido
     *
     * @param ctx
     * @param s
     */
    void saveScope(ParserRuleContext ctx, Scope s) {
        scopes.put(ctx, s);
    }

    /**
     * Muda para o contexto superior e atualia o escopo
     */
    private void popScope() {
        System.out.println("leaving: "+currentScope.getName()+":"+currentScope);
        currentScope = currentScope.getEnclosingScope();
    }

    public static void error(Token t, String msg) {
        System.err.printf("line %d:%d %s\n", t.getLine(), t.getCharPositionInLine(),
                msg);
    }

    /**
     * Valida tipos encontrados na linguagem para tipos reais
     *
     * @param tokenType
     * @return
     */
    public static DecafSymbol.Type getType(int tokenType) {
        switch ( tokenType ) {
            case DecafParser.VOID :  return DecafSymbol.Type.tVOID;
            case DecafParser.NUMBER :   return DecafSymbol.Type.tINT;
        }
        return DecafSymbol.Type.tINVALID;
    }


}