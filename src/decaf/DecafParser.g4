parser grammar DecafParser;

@header {
package decaf;
}

options
{
  language=Java;
  tokenVocab=DecafLexer;
}

program: TK_class LCURLY field_decl* method_decl* RCURLY EOF ;

field_decl: field_suport (VIRGULA field_suport)* P_VIRGULA ;

field_suport: decl_var | decl_var LCOCHETE NUMBER RCOCHETE  ;

method_decl: type ID LPARENT (decl_var(VIRGULA decl_var)*)? RPARENT block ;

decl_var: (INT | BOOLEAN)? ID ;

block: LCURLY var_decl* statement* RCURLY ;

var_decl: decl_var (VIRGULA decl_var)* P_VIRGULA ;

statement: location assign_op expr P_VIRGULA | method_call P_VIRGULA | IF LPARENT expr RPARENT block (ELSE block)? | FOR ID IGUAL expr VIRGULA expr block | RETURN expr? P_VIRGULA | BREAK P_VIRGULA   | CONTINUE P_VIRGULA | block  ;

assign_op: IGUAL | CONT_SOMA | CONT_SUB ;

method_call: method_name LPARENT (expr (VIRGULA expr)*)* RPARENT | CALLOUT LPARENT STRING (VIRGULA callout_arg)* RPARENT ;

//method_call: ID LPARENT (expr (VIRGULA expr)*)* RPARENT | CALLOUT LPARENT STRING (VIRGULA callout_arg)* RPARENT ;

method_name: ID ;

location: ID | ID LCOCHETE expr RCOCHETE ;

expr: location | method_call | literal | expr bin_op expr | SUBTRACAO expr | EXCLAMACAO expr | LPARENT expr* RPARENT ;

callout_arg: expr | STRING;

bin_op: ( arith_op | rel_op | eq_op | cond_op ) ;

arith_op: ( SOMA | SUBTRACAO | MULTPLI | DIVISAO | MOD );

rel_op: ( MENOR | MAIOR | MENOR_IGUAL | MAIOR_IGUAL );

eq_op: IGUAL_IGUAL | DIFERENTE;

cond_op: AND | OROR;

type: BOOLEAN | INT | VOID;

literal: ( NUMBER | BOOLEANLITERAL | CHARLITERAL );



