lexer grammar DecafLexer;

@header {
package decaf;
}

options
{
  language=Java;
}

TK_class : 'class Program';

EXCLAMACAO : '!';

LCURLY : '{';

RCURLY : '}';

P_VIRGULA: ';';

SOMA: '+';

SUBTRACAO: '-';

DIVISAO: '/';

MULTPLI: '*';

MOD: '%';

BARRA: '/';

MAIOR: '>';

MENOR: '<';

IGUAL: '=';

AND: '&&';

VIRGULA: ',';

LPARENT: '(';

RPARENT: ')';

LCOCHETE: '[';

RCOCHETE: ']';

OR: '|';

OROR: '||';

IGUAL_IGUAL: '==';

MENOR_IGUAL: '<=';

MAIOR_IGUAL: '>=';

CONT_SOMA: '+=';

CONT_SUB: '-=';

DIFERENTE: '!=';

INT : 'int';

BOOLEAN : 'boolean';

BREAK : 'break';

CALLOUT : 'callout';

CONTINUE : 'continue';

ELSE : 'else';

IF : 'if';

FOR : 'for';

RETURN : 'return';

VOID : 'void';

fragment FALSE : 'false';

BOOLEANLITERAL: FALSE | TRUE;

fragment TRUE: 'true';

ID  :  (LETRA | '_') IDD*;

NUMBER : HEXADEC | NUMERO (NUMERO)*;

WS_ : (' ' | '\n' | '\t') -> skip;

SL_COMMENT : '//' (~'\n')* '\n' -> skip;

STRING : '"' (ESC|CHAR)* '"';

CHARLITERAL : '\'' (ESC|CHAR) '\'';

fragment HEXADEC : '0x' HEXA+;

fragment HEXA: [a-fA-F0-9];

fragment IDD: (LETRA | NUMERO | '_');

fragment ESC :  '\\' ['n'|'"'|'t'|'\\'|'"'];

fragment LETRA : ('\u0041' .. '\u005a' | '\u0061' .. '\u007a')+;

fragment CHAR : ('\u0020' .. '\u0021' | '\u0023' .. '\u0026' | '\u0028' .. '\u005b' | '\u005d' .. '\u007e');

fragment NUMERO :('\u0030' .. '\u0039');
